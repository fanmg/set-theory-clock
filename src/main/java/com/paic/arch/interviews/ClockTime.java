package com.paic.arch.interviews;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ClockTime implements TimeConverter{
	@Override
	public String convertTime(String aTime){
		String regex = "^((20|21|22|23|[0-1]?\\d):[0-5]?\\d:[0-5]?\\d)$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(aTime);
		if(!matcher.matches()){
			return "date format is wrong!";
		}
		String[] tmp = aTime.split(":");
		return new Clock(Integer.parseInt(tmp[0]), Integer.parseInt(tmp[1]), Integer.parseInt(tmp[2])).toString();
	}
	/*
	@Test
	public void test(){
		//校验时钟的时灯显示是否正确
		for(int h=0;h<24;h++){
			String str = h+":00:00";
			System.out.println(str);
			System.out.println(this.convertTime(str));
		}
		//校验时钟的分灯显示是否正确
		for(int m=0;m<60;m++){
			String str = "00:"+m+":00";
			System.out.println(str);
			System.out.println(this.convertTime(str));
		}
		//校验时钟的秒灯显示是否正确
		for(int s=0;s<60;s++){
			String str = "00:00:"+s;
			System.out.println(str);
			System.out.println(this.convertTime(str));
		}
	}
	*/
	public static void main(String[] args){
		Date now = new Date();
		String aTime = new SimpleDateFormat("HH:m:ss").format(now);
		System.out.println("当前时间为: "+aTime+"\n时钟每排灯光显示为(R代表红灯,Y代表黄灯,B代表灯灭)如下:");
		System.out.println(new ClockTime().convertTime(aTime));
	}
}
