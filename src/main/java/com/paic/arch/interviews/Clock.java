package com.paic.arch.interviews;

public class Clock {
	//Red首写字母R代表红灯
	private final String rLight = "R";
	//Yellow首写字母Y代表黄灯
	private final String yLight = "Y";
	//Black首写字母B代表灯灭
	private final String bLight = "B";
	//存储时钟上的所有灯信息
	private String[][] lights;
	
	public Clock(int h,int m,int s){
		initClock();
		showTimeLight(h, m, s);
	}

	/** 
	  * @Title: initClock 
	  * @Description: 初始化时所有灯为灯灭状态
	  * @author meig.fan 
	  * void
	  */
	private void initClock(){
		lights = new String[][]{
			{bLight},
			{bLight,bLight,bLight,bLight},
			{bLight,bLight,bLight,bLight},
			{bLight,bLight,bLight,bLight,bLight,bLight,bLight,bLight,bLight,bLight,bLight},
			{bLight,bLight,bLight,bLight}
		};
	}
	
	/** 
	  * @Title: show5HourLight 
	  * @Description: 点亮每盏代表5小时的灯
	  * @author meig.fan 
	  * @param hour
	  * void
	  */
	private void show5HourLight(int hour){
		int toLightNum = hour/5;
		for(int i=0;i<toLightNum;i++){
			lights[1][i] = rLight;
		}
	}
	/** 
	  * @Title: showHourLight 
	  * @Description: 点亮每盏代表1小时的灯
	  * @author meig.fan 
	  * @param hour
	  * void
	  */
	private void showHourLight(int hour){
		int toLightNum = hour%5;
		for(int i=0;i<toLightNum;i++){
			lights[2][i] = rLight;
		}
	}
	/** 
	  * @Title: show5MinuteLight 
	  * @Description: 点亮每盏代表5分钟的灯
	  * @author meig.fan 
	  * @param minute
	  * void
	  */
	private void show5MinuteLight(int minute){
		int toLightNum = minute/5;
		for(int i=0;i<toLightNum;i++){
			lights[3][i] = (i+1)%3==0?rLight:yLight;
		}
	}
	/** 
	  * @Title: showMinuteLight 
	  * @Description: 点亮每盏代表1分钟的灯
	  * @author meig.fan 
	  * @param minute
	  * void
	  */
	private void showMinuteLight(int minute){
		int toLightNum = minute%5;
		for(int i=0;i<toLightNum;i++){
			lights[4][i] = yLight;
		}
	}
	/** 
	  * @Title: showSecondLight 
	  * @Description: 点亮每2秒亮一次的灯
	  * @author meig.fan 
	  * @param second
	  * void
	  */
	private void showSecondLight(int second){
		lights[0][0] = (second != 0 && second%2==0)?yLight:bLight;
	}
	
	/** 
	  * @Title: showTimeLight 
	  * @Description: 将时间转换为时钟的显示灯
	  * @author meig.fan 
	  * @param h
	  * @param m
	  * @param s
	  * void
	  */
	private void showTimeLight(int h,int m,int s){
		show5HourLight(h);
		showHourLight(h);
		show5MinuteLight(m);
		showMinuteLight(m);
		showSecondLight(s);
	}
	
	public String toString() {
		String result = "";
        if(this.lights != null){
        	for(String[] line:lights){
        		if(line != null){
        			for(String light:line){
        				result += light;
        			}
        		}
        		result += "\n";
        	}
        }
        return result;
    }

	public String[][] getLights() {
		return lights;
	}

	public void setLights(String[][] lights) {
		this.lights = lights;
	}
}
